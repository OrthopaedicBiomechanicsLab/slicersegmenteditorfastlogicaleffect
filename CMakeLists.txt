cmake_minimum_required(VERSION 3.13.4)

project(obl_tools)

#-----------------------------------------------------------------------------
# Extension meta-information
set(EXTENSION_HOMEPAGE "http://slicer.org/slicerWiki/index.php/Documentation/Nightly/Extensions/obl_tools")
set(EXTENSION_CATEGORY "Segmentation")
set(EXTENSION_CONTRIBUTORS "Yukun Zhang (OBL), Michael Hardisty(OBL)")
set(EXTENSION_DESCRIPTION "Faster version for segment editor")
set(EXTENSION_ICONURL "http://www.example.com/Slicer/Extensions/obl_tools.png")
set(EXTENSION_SCREENSHOTURLS "http://www.example.com/Slicer/Extensions/obl_tools/Screenshots/1.png")
set(EXTENSION_DEPENDS "NA") # Specified as a space separated string, a list or 'NA' if any

#-----------------------------------------------------------------------------
# Extension dependencies
find_package(Slicer REQUIRED)
include(${Slicer_USE_FILE})

#-----------------------------------------------------------------------------
# Extension modules
## NEXT_MODULE
add_subdirectory(SegmentEditorFastLogical)

#-----------------------------------------------------------------------------
include(${Slicer_EXTENSION_GENERATE_CONFIG})
include(${Slicer_EXTENSION_CPACK})