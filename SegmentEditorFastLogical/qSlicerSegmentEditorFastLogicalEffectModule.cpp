#include "qSlicerSegmentEditorFastLogicalEffectModule.h"

#include <qSlicerCoreApplication.h>

#include "vtkCollection.h"
#include "vtkMRMLScene.h"
#include "vtkMRMLSubjectHierarchyNode.h"

#include "qSlicerSubjectHierarchyPluginHandler.h"
#include "qSlicerSegmentEditorEffectFactory.h"

#include "qSlicerSegmentEditorFastLogicalEffect.h"

class qSlicerSegmentEditorFastLogicalEffectModulePrivate
{
	Q_DECLARE_PUBLIC(qSlicerSegmentEditorFastLogicalEffectModule);
protected:
	qSlicerSegmentEditorFastLogicalEffectModule* const q_ptr;
public:
	qSlicerSegmentEditorFastLogicalEffectModulePrivate(qSlicerSegmentEditorFastLogicalEffectModule& object);
	virtual ~qSlicerSegmentEditorFastLogicalEffectModulePrivate();
};

qSlicerSegmentEditorFastLogicalEffectModulePrivate::qSlicerSegmentEditorFastLogicalEffectModulePrivate(qSlicerSegmentEditorFastLogicalEffectModule& object)
	: q_ptr(&object)
{
}

qSlicerSegmentEditorFastLogicalEffectModulePrivate::~qSlicerSegmentEditorFastLogicalEffectModulePrivate()
= default;

qSlicerSegmentEditorFastLogicalEffectModule::qSlicerSegmentEditorFastLogicalEffectModule(QObject* _parent)
	:Superclass(_parent)
	, d_ptr(new qSlicerSegmentEditorFastLogicalEffectModulePrivate(*this))
{
}

qSlicerSegmentEditorFastLogicalEffectModule::~qSlicerSegmentEditorFastLogicalEffectModule()
= default;

QString qSlicerSegmentEditorFastLogicalEffectModule::helpText() const
{
	return "Improved version of logical operator effect to allow for faster update";
}

QString qSlicerSegmentEditorFastLogicalEffectModule::acknowledgementText() const
{
	return;
}

QStringList qSlicerSegmentEditorFastLogicalEffectModule::contributors() const
{
	QStringList moduleContributors;
	moduleContributors << QString("Jean-Christophe Fillion-Robin (Kitware)")
		<< QString("Csaba Pinter (PerkLab)")
		<< QString("Andras Lasso (PerkLab)")
		<< QString("Jean-Baptiste Vimort (Kitware)")
		<< QString("Adam Rankin (Robarts)");
	return moduleContributors;
}

QIcon qSlicerSegmentEditorFastLogicalEffectModule::icon() const
{
	return QIcon(":/Icons/Paint.png");
}

QStringList qSlicerSegmentEditorFastLogicalEffectModule::categories() const
{
	return QStringList() << "Fast Logical";
}

//-----------------------------------------------------------------------------
QStringList qSlicerSegmentEditorFastLogicalEffectModule::dependencies() const
{
	return QStringList() << "Segmentations";
}

//void qSlicerSegmentEditorFastLogicalEffectModule::setMRMLScene(vtkMRMLScene* scene)
//{
//	qvtkReconnect(this->mrmlScene(), scene, vtkMRMLScene::NodeAddedEvent, this, SLOT(onNodeAdded(vtkObject*, vtkObject*)));
//
//	Superclass::setMRMLScene(scene);
//	vtkCollection* shNodeCollection = scene->GetNodesByClass("vtkMRMLSubjectHierarchyNode");
//	vtkMRMLSubjectHierarchyNode* subjectHierarchyNode = vtkMRMLSubjectHierarchyNode::SafeDownCast(
//		shNodeCollection->GetItemAsObject(0));
//	shNodeCollection->Delete();
//	this->onNodeAdded(scene, subjectHierarchyNode);
//}

void qSlicerSegmentEditorFastLogicalEffectModule::setup()
{
	this->Superclass::setup();

	qSlicerAbstractCoreModule* segmentationsModule = 
		qSlicerCoreApplication::application()->moduleManager()->module("Segmentations");

	qSlicerSegmentEditorEffectFactory::instance()->registerEffect(new qSlicerSegmentEditorFastLogicalEffect());
}

qSlicerAbstractModuleRepresentation* qSlicerSegmentEditorFastLogicalEffectModule::createWidgetRepresentation()
{
	return;
}

vtkMRMLAbstractLogic* qSlicerSegmentEditorFastLogicalEffectModule::createLogic()
{
	return;
}