#ifndef __qSlicerSegmentEditorFastLogicalEffectModule_h
#define __qSlicerSegmentEditorFastLogicalEffectModule_h

#include "ctkVTKObject.h"

#include "qSlicerLoadableModule.h"
#include "qSlicerSegmentEditorFastLogicalEffectModuleExport.h"

class Q_SLICER_QTMODULES_SEGMENTEDITORFASTLOGICALEFFECT_EXPORT
	qSlicerSegmentEditorFastLogicalEffectModule : public qSlicerLoadableModule
{
	Q_OBJECT;
	Q_PLUGIN_METADATA(IID "org.slicer.modules.loadable.qSlicerLoadableModule/1.0");
	QVTK_OBJECT;
	Q_INTERFACES(qSlicerLoadableModule);

public:
	typedef qSlicerLoadableModule Superclass;
	explicit qSlicerSegmentEditorFastLogicalEffectModule(QObject* parent = 0);
	virtual ~qSlicerSegmentEditorFastLogicalEffectModule();

	qSlicerGetTitleMacro(QTMODULE_TITLE);

	virtual QString helpText()const;
	virtual QString acknowledgementText()const;
	virtual QStringList contributors()const;
	virtual QIcon icon()const;
	virtual QStringList categories()const;
	virtual QStringList dependencies() const;

//public slots:
//	virtual void setMRMLScene(vtkMRMLScene* scene);
//
protected:

	virtual void setup() override;
	
	vtkMRMLAbstractLogic* createLogic() override;

	qSlicerAbstractModuleRepresentation* createWidgetRepresentation() override;

protected:

	QScopedPointer<qSlicerSegmentEditorFastLogicalEffectModulePrivate> d_ptr;

private:
	Q_DECLARE_PRIVATE(qSlicerSegmentEditorFastLogicalEffectModule);
	Q_DISABLE_COPY(qSlicerSegmentEditorFastLogicalEffectModule);
};

#endif