#ifndef __qSlicerSegmentEditorFastLogicalEffect_h
#define __qSlicerSegmentEditorFastLogicalEffect_h

// Segmentations Editor Effects includes
#include "qSlicerSegmentEditorFastLogicalEffectExport.h"
#include "qSlicerSegmentEditorAbstractLabelEffect.h"
#include "qSlicerSegmentEditorEffectFactory.h"

class qSlicerSegmentEditorFastLogicalEffectPrivate;
class vtkPolyData;

class Q_SLICER_SEGMENTEDITORFASTLOGICALEFFECT_EXPORT
	qSlicerSegmentEditorFastLogicalEffect :	public qSlicerSegmentEditorAbstractLabelEffect
{
public:
	Q_OBJECT

public:
	typedef qSlicerSegmentEditorAbstractLabelEffect Superclass;
	qSlicerSegmentEditorFastLogicalEffect(QObject* parent = nullptr);
	~qSlicerSegmentEditorFastLogicalEffect() override;

public:
	/// Get icon for effect to be displayed in segment editor
	QIcon icon() override;

	/// Get help text for effect to be displayed in the help box
	Q_INVOKABLE const QString helpText()const override;
	
	/// Create options frame widgets, make connections, and add them to the main options frame using \sa addOptionsWidget
	void setupOptionsFrame() override;

	/// Set default parameters in the parameter MRML node
	void setMRMLDefaults() override;

	/// Clone editor effect
	qSlicerSegmentEditorAbstractEffect* clone() override;
	
	/// Callback function invoked when interaction happens
	/// \param callerInteractor Interactor object that was observed to catch the event
	/// \param eid Event identifier
	/// \param viewWidget Widget of the Slicer layout view. Can be \sa qMRMLSliceWidget or \sa qMRMLThreeDWidget
	bool processInteractionEvents(vtkRenderWindowInteractor* callerInteractor, unsigned long eid, qMRMLWidget* viewWidget) override;

	/// Perform actions to deactivate the effect (such as destroy actors, etc.)
	Q_INVOKABLE void deactivate() override;

public slots:
	void updateGUIFromMRML() override;

	void updateMRMLFromGUI() override;

	virtual void setOperation(int operationIndex);
	virtual void setShape(int shapeIndex);
	virtual void setSliceCutMode(int sliceCutModeIndex);
	virtual void onSliceCutDepthChanged(double value);

protected:
	QScopedPointer<qSlicerSegmentEditorFastLogicalEffectPrivate> d_ptr;
	void setup();

private:
	Q_DECLARE_PRIVATE(qSlicerSegmentEditorFastLogicalEffect);
	Q_DISABLE_COPY(qSlicerSegmentEditorFastLogicalEffect);
};

#endif
